package wapidstyle.notp;

import org.bukkit.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

// TODO COMPACT THIS CODE!
public class NoTP extends JavaPlugin {
	@Override
	public void onEnable(){
		getLogger().info("Successfully enabled NoTP 0.1!");
	}
	@Override
	public void onDisable(){
		getLogger().info("Successfully disabled NoTP 0.1!");
	}
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
		// TODO Add Essentials /tptoggle support.
		// TODO Add more types of tp'ing. (such as /tp 20 ~ ~1 or /tp 20 0 10)
		// TODO Add wildcards (such as @e)
		// TODO Add configurable messages.
		final Player affectp = Bukkit.getServer().getPlayerExact(sender.getName());
		final Player senderp = Bukkit.getServer().getPlayer(args[0]);
		final Permission p1 = new Permission("notp.tp");
		// final Permission p2 = new Permission("notp.tpothers"); To be implemented soon!
		final Permission p3 = new Permission("notp.tptoothers");
		String message = "";
		String cpg = ChatColor.DARK_GREEN + "[" + ChatColor.GREEN + "NoTP" + ChatColor.DARK_GREEN + "] " + ChatColor.GREEN;
		String cpb = ChatColor.DARK_RED + "[" + ChatColor.RED + "NoTP" + ChatColor.DARK_GREEN + "] " + ChatColor.RED;
		if(sender instanceof Player){
			if(senderp.hasPermission(p1) && affectp.hasPermission(p1)){
				if(senderp.hasPermission(p3)){
					senderp.teleport(affectp);
					message = cpg + " Successfully teleported to " + affectp.getDisplayName() + "!";
					senderp.sendMessage(message);
					message = "";
					return true;
				} else {
					message = cpb + " You can't teleport to others!";
					senderp.sendMessage(ChatColor.RED + "You are not allowed to teleport to others!");
					message = "";
					return true;
				}
			} else {
				if(senderp.hasPermission(p1)){
					message = cpb + " The person whom you tried to teleport to has teleporting disabled.";
					senderp.sendMessage(message);
					message = "";
					return true;
				} else {
					if(affectp.hasPermission(p1)){
						message = cpb + " You have teleporting disabled.";
						senderp.sendMessage(message);
						message = "";
						return false;
					} else {
						message = cpb + " Neither of you have teleporting enabled. D:";
						senderp.sendMessage(message);
						message = "";
						return false;
					}
				}
			}
		}
		return false;
	}
}
